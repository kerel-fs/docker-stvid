## Developer guide

### Install development dependencies

Install [docker](https://docs.docker.com/engine/install/) & [docker-compose](https://docs.docker.com/compose/install/).
For [building multi-platform images](https://github.com/docker/buildx#building-multi-platform-images),
install QEMU binaries for the target platforms & register them to `binfmt_misc`.


### Get the source code

```
git clone https://gitlab.com/librespacefoundation/docker-stvid
cd docker-stvid
ln -s docker-compose.dev.yml docker-compose.yml
```

### How to develop without re-building the container always?

During development of components inside the docker container it is preferable to bind-mount the source code directory
into the container. This can be done by using `docker-compose.dev.yml`:
```
docker-compose -f docker-compose.dev.yml stvid
```

### Build the image

```
docker-compose build stvid_acquire
```

The `stvid_acquire` and `stvid_process` service both use the same `stvid` container image.

### Usage

Follow the [User Guide](./user_guide.md#Usage) now.
