# User guide

## Installation of runtime dependencies

Install [docker](https://docs.docker.com/engine/install/) & [docker-compose](https://docs.docker.com/compose/install/).
If the installation of docker is not possible following the official documentation, you could follow
the steps of installation for Raspberry Pi 32-bit in we documented in [this guide](docker_install_guide.md).


## Environment selection

There are different contexts in which you want to run docker-stvid. Depending on the context different
service definitions are provided:
- docker-compose.dev.yml: Build your own images, bind-mount data & source code for easy development
- docker-compose.ci.yml: Build images in the CI environment

To select a specific definition you can use symlinks:
```
ln -s docker-compose.deploy.yml docker-compose.yml
```

### Dev environment

If you selected `docker-compose.dev.yml`, make sure to create the required directories:
```
mkdir -p example/obs example/tle example/obs_archive
```

## Usage

## Create your stvid configuration

- Copy the example configuration directory from `stvid-config.dist` to a new `stvid-config` directory:
  ```
  cp -r stvid-config.dist stvid-config
  ```

Then modify the configuration in `stvid-config/configuration.ini` as needed.

## Run stvid dummy acquire (only available with `docker-compose.dev.yml`)

Only available in dev env / when the docker-stvid source directory is bind mounted.
NOTE: examples folder is shared using git-lfs.

This command does copy example fits & tle files into the obs and tle directories:

```
docker-compose run stvid_acquire dummy-acquire pierros-2022-08-09
```

### Run stvid acquire

Run stvid acquire.

```
docker-compose up stvid_acquire
```


### Run stvid process

Run stvid process. This will automatically choose the latest stvid session directory.

```
docker-compose up stvid_process
```

## Update orbital elements

```
docker-compose run stvid_process update-tle
```

## Addition commands

### Delete process results (for re-processing)

This command deletes all files in the obs directory:

```
docker-compose run stvid_process delete-process-results
```
