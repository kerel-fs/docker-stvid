# STVID container image
#
# Copyright (C) 2022 Libre Space Foundation <https://libre.space/>
# SPDX-License-Identifier: AGPL-3.0

FROM debian:bullseye
LABEL org.opencontainers.image.authors='SatNOGS project <dev@satnogs.org>'

ARG VERSION
ARG STVID_UID=999
ARG STVID_VARSTATEDIR=/var/lib/stvid
ARG STVID_SRCDIR=/usr/local/src/stvid

# Ensure python output appears swiftly in logs
ENV PYTHONUNBUFFERED 1

# Install system packages
COPY packages.debian /usr/local/src/docker-stvid/
COPY packages-astrometry.debian /usr/local/src/docker-stvid/
RUN apt-get update \
	&& xargs -a /usr/local/src/docker-stvid/packages.debian apt-get install -qy \
	&& xargs -a /usr/local/src/docker-stvid/packages-astrometry.debian apt-get install -qy \
	&& rm -r /var/lib/apt/lists/*

# Workaround: re-add old command sextractor
RUN ln -s /usr/bin/source-extractor /usr/local/bin/sextractor

# Install python packages (which are unavailable via apt)
RUN pip3 install zwoasi~=0.1.0.1 picamerax~=21.9.8 spacetrack~=0.16.0 --prefer-binary

# Install hough3d-code
RUN git clone https://gitlab.com/pierros/hough3d-code.git /hough3d-code &&\
	cd /hough3d-code &&\
	make all &&\
	cp -p hough3dlines /usr/local/bin/hough3dlines &&\
	cd / && rm -rf /hough3d-code

# Install satpredict
RUN git clone https://github.com/cbassa/satpredict /satpredict &&\
	cd /satpredict &&\
	make &&\
	make install &&\
	cd / && rm -rf /satpredict

# Install libasi
RUN apt-get update &&\
    apt-get install -qy cmake &&\
	rm -r /var/lib/apt/lists/*

RUN git clone --depth 1 --filter=blob:none --sparse https://github.com/indilib/indi-3rdparty.git /indi-3rdparty &&\
    cd /indi-3rdparty &&\
    git sparse-checkout set libasi cmake_modules/InstallImported.cmake &&\
    sed -i '2s/.*/project (libasi NONE)/' libasi/CMakeLists.txt &&\
    mkdir libasi/build &&\
    cd libasi/build &&\
    cmake -DCMAKE_INSTALL_PREFIX="/usr/local" ../../libasi &&\
    make &&\
    make install &&\
    cd / && rm -rf /indi-3rd-party

# Add unprivileged system user
RUN groupadd -r -g ${STVID_UID} stvid \
	&& useradd -r -u ${STVID_UID} \
		-g stvid \
		-d ${STVID_VARSTATEDIR} \
		-s /usr/bin/false \
		-G audio,dialout,plugdev \
		stvid

# Create application varstate directory
RUN install -d -o ${STVID_UID} -g ${STVID_UID} ${STVID_VARSTATEDIR}

# Download source code
RUN git clone -c advice.detachedHead=false --depth 1 -b ${VERSION} https://github.com/kerel-fs/stvid.git ${STVID_SRCDIR}

# Install scripts
COPY bin /usr/local/src/docker-stvid/bin

RUN ln -s /usr/local/src/docker-stvid/bin/delete-process-results /usr/local/bin/delete-process-results &&\
    ln -s /usr/local/src/docker-stvid/bin/dummy-acquire /usr/local/bin/dummy-acquire &&\
    ln -s /usr/local/src/docker-stvid/bin/acquire /usr/local/bin/acquire &&\
    ln -s /usr/local/src/docker-stvid/bin/process-latest /usr/local/bin/process-latest &&\
    ln -s /usr/local/src/docker-stvid/bin/update-tle /usr/local/bin/update-tle
